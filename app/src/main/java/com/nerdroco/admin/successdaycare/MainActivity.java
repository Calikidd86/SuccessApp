package com.nerdroco.admin.successdaycare;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private LinearLayout nameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);  // change back from splash screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        nameList = findViewById(R.id.childList);
    }

    public void checkIn(View view) {
    }

    public void addChild(View view) {
        Intent intent = new Intent(this, AddChildActivity.class);
        startActivity(intent);
//        TextView child = new TextView(this);
//        child.setText("Whats up");
//        nameList.addView(child);
    }
}
