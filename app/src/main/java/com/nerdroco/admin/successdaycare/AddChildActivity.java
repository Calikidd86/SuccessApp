package com.nerdroco.admin.successdaycare;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddChildActivity extends AppCompatActivity {

    public static final String EXTRA_FNAME =
            "com.nerdroco.admin.successdaycare.AddChild.extra.fname";

    public static final String EXTRA_LNAME =
            "com.nerdroco.admin.successdaycare.AddChild.extra.lname";

    public static final String EXTRA_AGE =
            "com.nerdroco.admin.successdaycare.AddChild.extra.age";

    private EditText f_name;
    private EditText l_name;
    private EditText age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);
    }


    public void add(View view) {
        f_name = findViewById(R.id.f_nameText);
        l_name = findViewById(R.id.l_nameText);
        age = findViewById(R.id.ageText);

        String firstName = f_name.getText().toString();
        String lastName = l_name.getText().toString();

        if (TextUtils.isEmpty(age.getText())){
            age.setText(0);
            Toast.makeText(this, "Age is required", Toast.LENGTH_LONG).show();
        } else {

            int ageValue = Integer.parseInt(age.getText().toString());

            Intent replyIntent = new Intent();
            replyIntent.putExtra(EXTRA_FNAME, firstName);
            replyIntent.putExtra(EXTRA_LNAME, lastName);
            replyIntent.putExtra(EXTRA_AGE, ageValue);
            setResult(RESULT_OK, replyIntent);
            finish();
        }
    }
}
